import numpy as np
import cv2
import glob
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from moviepy.editor import VideoFileClip

def mag_thresh(img, thresh):
    # 1) Convert the image to Grayscale
    gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
    # 2) Take the gradient in x and y separately
    sobelx = cv2.Sobel(gray, cv2.CV_64F, 1, 0)
    sobely = cv2.Sobel(gray, cv2.CV_64F, 0, 1)
    # 3) Calculate the magnitude
    mag_sobel = np.sqrt(np.square(sobelx) + np.square(sobely))
    # 4) Scale to 8-bit (0 - 255) and convert to type = np.uint8
    scaled_sobel = np.uint8(255 * mag_sobel / np.max(mag_sobel))
    # 5) Create a binary mask where mag thresholds are met
    sxbinary = np.zeros_like(scaled_sobel)
    sxbinary[(scaled_sobel >= thresh[0]) & (scaled_sobel <= thresh[1])] = 1
    # 6) Return this mask as your binary_output image
    return sxbinary

def select_yellow(image):
    hsv = cv2.cvtColor(image, cv2.COLOR_RGB2HSV)
    lower = np.array([0, 80, 200])
    upper = np.array([40, 255, 255])
    mask = cv2.inRange(hsv, lower, upper)

    return mask

def select_white(image):
    lower = np.array([200, 200, 200])
    upper = np.array([255, 255, 255])
    mask = cv2.inRange(image, lower, upper)
    return mask


def lgt_threshold(img, thresh):
    new_img = cv2.cvtColor(img, cv2.COLOR_RGB2LUV)
    luv = new_img[:, :, 0]
    l_binary = np.zeros_like(luv)
    l_binary[(luv >= thresh[0]) & (luv <= thresh[1])] = 1
    return l_binary

def color_thresh(img, thresh):
    #new_img = cv2.cvtColor(img, cv2.COLOR_RGB2HLS)
    sat = img[:, :, 2]
    s_binary = np.zeros_like(sat)
    s_binary[((sat > thresh[0]) & (sat <= thresh[1]))] = 1
    return s_binary

''' Apply each of the thresholding functions '''
def threshold_comb(image):

    mag_binary = mag_thresh(image, thresh=(10, 255))

    yellow_bin = select_yellow(image)
    white_bin = select_white(image)

    combined_warped_binary = cv2.bitwise_or(yellow_bin, white_bin)
    combined_binary = cv2.bitwise_or(combined_warped_binary, mag_binary)
    return combined_binary

''' Apply perspective transform to the top view '''
def get_topview(img, objpoints, imgpoints):
    img_size = (img.shape[1], img.shape[0])
    ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, img_size, None, None)
    undist_img = cv2.undistort(img, mtx, dist, None, mtx)

    offset = 0  # offset for dst points
    # Grab the image shape
    img_size = (undist_img.shape[1], undist_img.shape[0])

    dst = np.float32([[offset, offset], [img_size[0] - offset, offset],
                      [img_size[0] - offset, img_size[1] - offset],
                      [offset, img_size[1] - offset]])
    src = np.float32([[490, 472], [810, 472],
                      [1250, 720], [40, 720]])
    # Given src and dst points, calculate the perspective transform matrix
    M = cv2.getPerspectiveTransform(src, dst)
    Minv = cv2.getPerspectiveTransform(dst, src)
    # Warp the image using OpenCV warpPerspective()
    warped = cv2.warpPerspective(undist_img, M, img_size)
    return warped, Minv, undist_img

def calibrate_cam():

    images = glob.glob("camera_cal/calibration*.jpg")
    objpoints = [] # 3D points in real world space
    imgpoints = [] # 2D points in image space
    objp = np.zeros((9*6,3), np.float32)
    objp[:, :2] = np.mgrid[0:9, 0:6].T.reshape(-1, 2)
    for fname in images:
        img = cv2.imread(fname)
        # Convert to grayscale
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        # Find the chessboard corners
        ret, corners = cv2.findChessboardCorners(gray, (9, 6), None)
        # If found, add object points, image points
        if ret == True:
            objpoints.append(objp)
            imgpoints.append(corners)

            # Draw and display the corners
            cv2.drawChessboardCorners(img, (9, 6), corners, ret)

    return objpoints, imgpoints

curvature = 600
rightfit_prev = 1

''' Main Pipeline to fit a polynomial based on the points of lane lines '''
def fitpoly(binary_warped, Minv, undist, warped):
    # Assuming you have created a warped binary image called "binary_warped"
    # Take a histogram of the bottom half of the image
    histogram = np.sum(binary_warped[binary_warped.shape[0]//2:, :], axis=0)
    # Create an output image to draw on and  visualize the result
    out_img = np.dstack((binary_warped, binary_warped, binary_warped)) * 255
    # Find the peak of the left and right halves of the histogram
    # These will be the starting point for the left and right lines
    midpoint = np.int(histogram.shape[0] / 2)
    leftx_base = np.argmax(histogram[:midpoint])
    rightx_base = np.argmax(histogram[midpoint:]) + midpoint

    # plt.imshow(undist)
    # plt.xlim(0, 1280)
    # plt.ylim(720, 0)
    # plt.show()
    # plt.imshow(binary_warped)
    # plt.xlim(0, 1280)
    # plt.ylim(720, 0)
    # plt.show()


    # Choose the number of sliding windows
    nwindows = 4

    # Set height of windows
    window_height = np.int(binary_warped.shape[0] / nwindows)
    # Identify the x and y positions of all nonzero pixels in the image
    nonzero = binary_warped.nonzero()
    nonzeroy = np.array(nonzero[0])
    nonzerox = np.array(nonzero[1])

    # Current positions to be updated for each window
    leftx_current = leftx_base
    rightx_current = rightx_base
    # Set the width of the windows +/- margin
    margin = 100
    # Set minimum number of pixels found to recenter window
    minpix = 50
    # Create empty lists to receive left and right lane pixel indices
    left_lane_inds = []
    right_lane_inds = []

    # Step through the windows one by one
    for window in range(nwindows):
        # Identify window boundaries in x and y (and right and left)
        win_y_low = binary_warped.shape[0] - (window + 1) * window_height
        win_y_high = binary_warped.shape[0] - window * window_height
        win_xleft_low = leftx_current - margin
        win_xleft_high = leftx_current + margin
        win_xright_low = rightx_current - margin
        win_xright_high = rightx_current + margin
        # Draw the windows on the visualization image
        cv2.rectangle(out_img, (win_xleft_low, win_y_low), (win_xleft_high, win_y_high), (0, 255, 0), 2)
        cv2.rectangle(out_img, (win_xright_low, win_y_low), (win_xright_high, win_y_high), (0, 255, 0), 2)
        # Identify the nonzero pixels in x and y within the window
        good_left_inds = ((nonzeroy >= win_y_low) & (nonzeroy < win_y_high) & (nonzerox >= win_xleft_low) & (
        nonzerox < win_xleft_high)).nonzero()[0]
        good_right_inds = ((nonzeroy >= win_y_low) & (nonzeroy < win_y_high) & (nonzerox >= win_xright_low) & (
        nonzerox < win_xright_high)).nonzero()[0]
        # Append these indices to the lists
        left_lane_inds.append(good_left_inds)
        right_lane_inds.append(good_right_inds)
        # If you found > minpix pixels, recenter next window on their mean position
        if len(good_left_inds) > minpix:
            leftx_current = np.int(np.mean(nonzerox[good_left_inds]))
        if len(good_right_inds) > minpix:
            rightx_current = np.int(np.mean(nonzerox[good_right_inds]))

    # Concatenate the arrays of indices
    left_lane_inds = np.concatenate(left_lane_inds)
    right_lane_inds = np.concatenate(right_lane_inds)

    # Extract left and right line pixel positions

    leftx = nonzerox[left_lane_inds]
    lefty = nonzeroy[left_lane_inds]
    rightx = nonzerox[right_lane_inds]
    righty = nonzeroy[right_lane_inds]

    # Fit a second order polynomial to each
    left_fit = np.polyfit(lefty, leftx, 2)
    right_fit = np.polyfit(righty, rightx, 2)

    ploty = np.linspace(0, binary_warped.shape[0] - 1, binary_warped.shape[0])
    left_fitx = left_fit[0] * ploty ** 2 + left_fit[1] * ploty + left_fit[2]
    right_fitx = right_fit[0] * ploty ** 2 + right_fit[1] * ploty + right_fit[2]



    out_img[nonzeroy[left_lane_inds], nonzerox[left_lane_inds]] = [255, 0, 0]
    out_img[nonzeroy[right_lane_inds], nonzerox[right_lane_inds]] = [0, 0, 255]

    warp_zero = np.zeros_like(binary_warped).astype(np.uint8)
    color_warp = np.dstack((warp_zero, warp_zero, warp_zero))

    # Recast the x and y points into usable format for cv2.fillPoly()
    pts_left = np.array([np.transpose(np.vstack([left_fitx, ploty]))])
    pts_right = np.array([np.flipud(np.transpose(np.vstack([right_fitx, ploty])))])
    pts = np.hstack((pts_left, pts_right))
    cv2.polylines(color_warp, np.int_([pts]), isClosed=True, color=(255, 0, 255), thickness=60)

    # Draw the lane onto the warped blank image
    cv2.fillPoly(color_warp, np.int_([pts]), (64, 224, 208))

    y_eval = np.max(ploty)
    # Correction for y_eval
    ym_per_pix = 30 / 720  # meters per pixel in y dimension
    xm_per_pix = 3.7 / 700  # meters per pixel in x dimension
    left_fit_cr = np.polyfit(ploty * ym_per_pix, left_fitx * xm_per_pix, 2)
    left_curverad = ((1 + (2 * left_fit_cr[0] * y_eval * ym_per_pix + left_fit_cr[1]) ** 2) ** 1.5) / np.absolute(
        2 * left_fit_cr[0])


    # Warp the blank back to original image space using inverse perspective matrix (Minv)
    newwarp = cv2.warpPerspective(color_warp, Minv, (undist.shape[1], undist.shape[0]))
    # Combine the result with the original image
    result = cv2.addWeighted(undist, 1, newwarp, 0.3, 0)


    text = "Radius of Curvature: {} m".format(int(left_curverad))
    cv2.putText(result, text, (400, 100), cv2.FONT_HERSHEY_PLAIN, 2, (255, 255, 255), 2)

    camera_position = undist.shape[1] / 2
    pts = np.argwhere(newwarp[:, :, 1])
    left = np.min(pts[(pts[:, 1] < camera_position) & (pts[:, 0] > 700)][:, 1])
    right = np.max(pts[(pts[:, 1] > camera_position) & (pts[:, 0] > 700)][:, 1])
    center = (left + right) / 2
    center_offset_pixels = abs(camera_position - center)*xm_per_pix
    text = "Position from Center: {} m".format(int(center_offset_pixels))
    cv2.putText(result, text, (400, 150), cv2.FONT_HERSHEY_PLAIN, 2, (255, 255, 255), 2)

    # plt.imshow(result)
    # plt.imshow(binary_warped)
    # plt.plot(left_fitx, ploty, color='green')
    # plt.plot(right_fitx, ploty, color='green')
    # plt.xlim(0, 1280)
    # plt.ylim(720, 0)
    # plt.show()
    return result

''' Call the steps of pipeline here '''
def pipeline(image):

    objpoints, imgpoints = calibrate_cam()
    img, Minv, undist = get_topview(image, objpoints, imgpoints)
    binary_warped = threshold_comb(img)
    return fitpoly(binary_warped, Minv, undist, img)


''' The main function starts here '''
def main():
    # for i in [16]:
    #     img = mpimg.imread('test_images/test' + str(i) + '.jpg')
    #     pipeline(img)
    video_output = 'challenge_result.mp4'
    clip1 = VideoFileClip("challenge_video.mp4")
    white_clip = clip1.fl_image(pipeline)
    white_clip.write_videofile(video_output, audio=False)

if __name__ == '__main__':
    main()